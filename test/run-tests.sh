#! /usr/bin/env bash

# by torstein@escenic.com

set -o errexit
set -o nounset
set -o pipefail
shopt -s nullglob

id="[$(basename $0)]"

read_user_input() {
  if [ $# -lt 1 ]; then
    return
  fi

  if [ $1 ]; then
    test_script=${1}
  fi
}

print() {
  echo "${id} $*"
}

run_tests() {
  for el in $(dirname $0)/*test.sh; do
    print "Running ${el} ..."
    local shell=bash
    (
      exec ${shell} ${el} 2>&1
    )
  done
}

ensure_shunit_is_available() {
  (
    cd "$(dirname "$0")"

    if [ ! -d shunit2 ]; then
      print "Downloading shunit ..."
      git clone https://github.com/kward/shunit2.git
    fi
  )
}

main() {
  read_user_input "$@"
  ensure_shunit_is_available
  run_tests
}

main "$@"
