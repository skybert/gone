#! /usr/bin/env bash

# by torstein@escenic.com

## @override shunit2
setUp() {
  source "$(dirname "$0")/../lib/$(basename "$0" -test.sh)-lib.sh"
}

## @override shunit2
tearDown() {
  :
}

test_null_somehing() {
  local var=" "
  assertNotNull "${var}"
}

test_add_3_to_2_yields_5() {
  local expected=5
  local actual=
  actual=$(add_two 3)
  assertEquals "${expected}" "${actual}"
}

test_add_6_to_2_yields_8() {
  local expected=8
  local actual=
  actual=$(add_two 6)
  assertEquals "${expected}" "${actual}"
}

test_add_2_to_nothing_return_2() {
  local expected=2
  local actual=
  actual=$(add_two)
  assertEquals "${expected}" "${actual}"
}

main() {
  source "$(dirname "$0")"/shunit2/source/2.1/src/shunit2
}

main "$@"
